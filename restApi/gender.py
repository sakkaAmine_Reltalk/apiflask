from keras.models import load_model
import tensorflow as tf

#loading the genderModel
TARGET_SHAPE = (224,224,3)
Threshold=70
genderModel = load_model("./models/gender_classification_model.h5")

def predictGeneder(imagePath):
    image_string = tf.io.read_file(imagePath)
    # print(image_string)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = tf.image.resize(image, TARGET_SHAPE[:2])
    image = tf.expand_dims(image, axis=0)
    genderScore = genderModel.predict(image)[0][0] * 100
    print(genderScore)
    gender = ""
    print(genderScore)
    if genderScore >= Threshold:
        gender = "female"
    else:
        gender = "male"
    print(gender)

    return gender



