from flask import Flask, jsonify, request
from flask_restful import Api, Resource
import urllib.request
import os

app = Flask(__name__)
api = Api(app)


def saveIamges(listOfImages , dir_name):
    os.mkdir(dir_name)
    for imageData in listOfImages:
        imageId=imageData["id"]
        imageUrl=imageData["url"]
        imageName=str(imageId)+".jpg"
        saveName=dir_name+"/"+imageName
        urllib.request.urlretrieve(imageUrl, saveName)
        print(imageName)

def saveIamge(imageUrl,imageName):
    saveName ="./ToPredict/" + imageName+".jpg"
    urllib.request.urlretrieve(imageUrl, saveName)

class Images(Resource):
    def post(self):
        postedData = request.get_json()
        # Get the data
        listOfImages = postedData["images"]
        catalogue=postedData["catalogue"]
        saveIamges(listOfImages,"./data/"+catalogue)
        print(listOfImages)
        retJson = {
            "status": 200,
            "msg": "images where saved"
        }
        return jsonify(retJson)

class Train(Resource):
    def post(self):

        retJson = {
            "status": 200,
            "msg": "train completed"
        }
        return jsonify(retJson)

class Predict(Resource):
    def post(self):
        postedData = request.get_json()
        imageUrl = postedData["image"]
        imageName = postedData["imageName"]

        retJson = {
            "status": 200,
            "msg": "image prediction completed"
        }
        return jsonify(retJson)


api.add_resource(Images, '/images')
api.add_resource(Train, '/train')
api.add_resource(Predict, '/predict')


if __name__ == '__main__':
    app.run(host='0.0.0.0')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
