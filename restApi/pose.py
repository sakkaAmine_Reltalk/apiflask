import cv2
import numpy as np

# Loading the pose model
TOP_BODY_PARTS = {"Nose": 0, "REye": 14, "LEye": 15, }
BOTTOM_BODY_PARTS = {"RAnkle": 10, "LAnkle": 13}
NETWORK = cv2.dnn.readNetFromTensorflow("./models/human_pose_estimation.pb")
THRESHOLD = 0.25
REQUIRED_OUTPUTS = list(TOP_BODY_PARTS.values()) + list(BOTTOM_BODY_PARTS.values())

def is_fullpose(image: np.ndarray):
    NETWORK.setInput(cv2.dnn.blobFromImage(image, 1.0, (368, 368), (127.5, 127.5, 127.5), swapRB=True, crop=False))
    out = NETWORK.forward()[:, REQUIRED_OUTPUTS, :, :]
    top, bottom = False, False
    for i in range(0, 3):
        heatMap = out[0, i, :, :]
        _, confidence, _, _ = cv2.minMaxLoc(heatMap)
        if confidence > THRESHOLD:
            top = True
            break
    for i in range(3, 5):
        heatMap = out[0, i, :, :]
        _, confidence, _, _ = cv2.minMaxLoc(heatMap)
        if confidence > THRESHOLD:
            bottom = True
            break

    return top and bottom

def poseModel(imagePath):
    if is_fullpose(imagePath):
        title = "Full Pose Image"
    else:
        title = "Not a Full Pose Image"

    return title